﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Readify.WebSerivce.Extension
{
    public static class MathService
    {
        private readonly static double GoldenRation = (1.0 + Math.Sqrt(5.0)) / 2.0;

        public static long Fibonacci(long n)
        {
            if (n < 0) 
                return (long)Math.Pow(-1, -n + 1) * Fibonacci(-n);

            return (long)Math.Floor((Math.Pow(GoldenRation, n) - Math.Pow(1 - GoldenRation, n)) / Math.Sqrt(5.0));            
        }

    }
}
