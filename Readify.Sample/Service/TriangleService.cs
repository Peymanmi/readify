﻿namespace Readify.WebSerivce.Service
{
    using Enums;

    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class TriangleService 
    {
        public static TriangleType FindShapeType(int a, int b, int c)
        {
            var vertexes = new[] { a, b, c };

            if (!ValidateVertexes(vertexes))
                return TriangleType.Error;

            var distinctCount = vertexes.Distinct().Count();

            switch(distinctCount)
            {
                case 1:
                    return TriangleType.Equilateral;
                case 2:
                    return TriangleType.Isosceles;
                default:
                    return TriangleType.Scalene;
            }
        }

        public static bool ValidateVertexes(int[] inputs)
        {
            if (inputs.Any(x => x == 0))
                return false;

            var result = true;

            for (int indx = 0; indx < inputs.Length; indx++)
            {
                var sides = inputs.Take(indx).Concat(inputs.Skip(indx + 1))
                    .Select(x => (double)x);
                result &= sides.Sum() > inputs[indx];
                if (!result)
                    return false;
            }

            return result;
        }
    }
}
