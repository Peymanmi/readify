﻿namespace Readify.WebSerivce
{
    using Enums;
    using Extension;
    using Service;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.Text;

    [ServiceBehavior(Namespace = "http://KnockKnock.readify.net")]
    public class RedPill : IRedPill
    {

        public Guid WhatIsYourToken()
        {
            //return Guid.Empty;
            return new Guid("3f58e255-4cdd-4a53-be3d-bcdd0841e205");
        }

        public long FibonacciNumber(long n)
        {
            if (!(-92 <= n && n <= 92)) 
                Fault(new ArgumentOutOfRangeException("n", "Require 0 <= n <= 92"));

            return MathService.Fibonacci(n);
        }

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            return TriangleService.FindShapeType(a, b, c);
        }

        public string ReverseWords(string s)
        {
            if (s == null)
                Fault(new ArgumentNullException("s", "Require s != null"));

            return s.ReverseWord();
        }

        void Fault<T>(T detail) where T : Exception
        {
            throw new FaultException<T>(detail, detail.Message);
        }
    }
}
