﻿namespace Readify.WebSerivce.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public static class StringExtensions
    {
        public static string ReverseWord(this string input)
        {
            return input.ReverseWord(new[] { ' ' });
        }

        public static string ReverseWord(this string input, char[] separators)
        {
            if (input == null)
                throw new ArgumentNullException("s", "Value can not be null.");

            var words = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            var reversedArray = new List<string>();
            words.ToList().ForEach(str =>
            {
                var chArray = str.ToCharArray();
                Array.Reverse(chArray);

                reversedArray.Add(new string(chArray));
            });

            return string.Join(" ", reversedArray);
        }
    }
}
