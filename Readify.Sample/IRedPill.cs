﻿namespace Readify.WebSerivce
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Security;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;


    [ServiceContract(Namespace = "http://KnockKnock.readify.net")]
    public interface IRedPill
    {
        [OperationContract, FaultContract(typeof(ArgumentOutOfRangeException))]
        long FibonacciNumber(long n);
        [OperationContract, FaultContract(typeof(ArgumentNullException))]
        string ReverseWords(string s);
        [OperationContract]
        TriangleType WhatShapeIsThis(int a, int b, int c);
        [OperationContract]
        Guid WhatIsYourToken();
    }
}
