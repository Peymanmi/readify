﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Readify.ServiceTest
{
    [TestClass]
    public sealed class ServiceTester : IDisposable
    {
        private ReadifyServices.RedPillClient _service;
        private ReadifyReferences.RedPillClient _reference;

        public ServiceTester()
        {
            Initial();
        }

        public void Initial()
        {
            _service = new ReadifyServices.RedPillClient();
            _service.Open();

            _reference = new ReadifyReferences.RedPillClient();
            _reference.Open();
        }

        [TestMethod]
        public void Validate_Triangle_Multi_Test_Must_Return_Error()
        {
            var result1 = _service.WhatShapeIsThis(0, 0, 0);
            var result2 = _reference.WhatShapeIsThis(0, 0, 0);

            Assert.AreEqual((int)result1, (int)result2);


            result1 = _service.WhatShapeIsThis(1, 1, 0);
            result2 = _reference.WhatShapeIsThis(1, 1, 0);

            Assert.AreEqual((int)result1, (int)result2);


            result1 = _service.WhatShapeIsThis(1, 1, 2);
            result2 = _reference.WhatShapeIsThis(1, 1, 2);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(1, 0, 1);
            result2 = _reference.WhatShapeIsThis(1, 0, 1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(2, 1, 1);
            result2 = _reference.WhatShapeIsThis(2, 1, 1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(1, 2, 3);
            result2 = _reference.WhatShapeIsThis(1, 2, 3);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(1, 1, 2147483647);
            result2 = _reference.WhatShapeIsThis(1, 1, 2147483647);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(1, 1, 1);
            result2 = _reference.WhatShapeIsThis(1, 1, 1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(2, 2, 2);
            result2 = _reference.WhatShapeIsThis(2, 2, 2);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(2147483647, 2147483647, 2147483647);
            result2 = _reference.WhatShapeIsThis(2147483647, 2147483647, 2147483647);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(2, 2, 3);
            result2 = _reference.WhatShapeIsThis(2, 2, 3);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(2, 3, 2);
            result2 = _reference.WhatShapeIsThis(2, 3, 2);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(3, 2, 2);
            result2 = _reference.WhatShapeIsThis(3, 2, 2);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(2, 3, 4);
            result2 = _reference.WhatShapeIsThis(2, 3, 4);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(3, 4, 2);
            result2 = _reference.WhatShapeIsThis(3, 4, 2);

            Assert.AreEqual((int)result1, (int)result2);


            result1 = _service.WhatShapeIsThis(4, 2, 3);
            result2 = _reference.WhatShapeIsThis(4, 2, 3);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(4, 3, 2);
            result2 = _reference.WhatShapeIsThis(4, 3, 2);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(1, 2, 1);
            result2 = _reference.WhatShapeIsThis(1, 2, 1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(-2147483647, -2147483647, -2147483647);
            result2 = _reference.WhatShapeIsThis(-2147483647, -2147483647, -2147483647);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(-1, -1, -1);
            result2 = _reference.WhatShapeIsThis(-1, -1, -1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(-1, 1, 1);
            result2 = _reference.WhatShapeIsThis(-1, 1, 1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(1, -1, 1);
            result2 = _reference.WhatShapeIsThis(1, -1, 1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(1, 1, -1);
            result2 = _reference.WhatShapeIsThis(1, 1, -1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(0, 1, 1);
            result2 = _reference.WhatShapeIsThis(0, 1, 1);

            Assert.AreEqual((int)result1, (int)result2);

            result1 = _service.WhatShapeIsThis(2147483647, 2147483647, 2147483647);
            result2 = _reference.WhatShapeIsThis(2147483647, 2147483647, 2147483647);

            Assert.AreEqual((int)result1, (int)result2);
        }

        [TestMethod]
        public void Validate_Triangle_Must_Return_Error()
        {
            var result1 = _service.WhatShapeIsThis(2, 5, 12);
            var result2 = _reference.WhatShapeIsThis(2, 5, 12);

            Assert.AreEqual((int)result1, (int)result2);

            Assert.AreEqual(result1, Readify.ServiceTest.ReadifyServices.TriangleType.Error);
        }

        [TestMethod]
        public void Validate_Triangle_With_Negative_Must_Return_Error()
        {
            var result1 = _service.WhatShapeIsThis(-1, -1, -1);
            var result2 = _reference.WhatShapeIsThis(-1, -1, -1);

            Assert.AreEqual((int)result1, (int)result2);

            Assert.AreEqual(result1, Readify.ServiceTest.ReadifyServices.TriangleType.Error);
        }

        [TestMethod]
        public void Validate_Triangle_With_Zero_Must_Return_Error()
        {
            var result1 = _service.WhatShapeIsThis(0, 1, 1);
            var result2 = _reference.WhatShapeIsThis(0, 1, 1);

            Assert.AreEqual((int)result1, (int)result2);

            Assert.AreEqual(result1, Readify.ServiceTest.ReadifyServices.TriangleType.Error);
        }

        [TestMethod]
        public void Validate_Triangle_With_Large_Value_Must_Return_Error()
        {
            var result2 = _reference.WhatShapeIsThis(-2147483647, -2147483647, -2147483647);
            var result1 = _service.WhatShapeIsThis(-2147483647, -2147483647, -2147483647);

            Assert.AreEqual((int)result1, (int)result2);

            Assert.AreEqual(result1, Readify.ServiceTest.ReadifyServices.TriangleType.Error);
        }

        [TestMethod]
        public void Validate_Triangle_Must_Return_Equilateral()
        {
            var result1 = _service.WhatShapeIsThis(5, 5, 5);
            var result2 = _reference.WhatShapeIsThis(5, 5, 5);

            Assert.AreEqual((int)result1, (int)result2);

            Assert.AreEqual(result1, Readify.ServiceTest.ReadifyServices.TriangleType.Equilateral);
        }

        [TestMethod]
        public void Validate_Triangle_Must_Return_Isosceles()
        {
            var result1 = _service.WhatShapeIsThis(5, 5, 3);
            var result2 = _reference.WhatShapeIsThis(5, 5, 3);

            Assert.AreEqual((int)result1, (int)result2);
            Assert.AreEqual(result1, Readify.ServiceTest.ReadifyServices.TriangleType.Isosceles);
        }

        [TestMethod]
        public void Validate_Triangle_Must_Return_Scalene()
        {
            var result1 = _service.WhatShapeIsThis(3, 5, 7);
            var result2 = _reference.WhatShapeIsThis(3, 5, 7);

            Assert.AreEqual((int)result1, (int)result2);

            Assert.AreEqual(result1, Readify.ServiceTest.ReadifyServices.TriangleType.Scalene);
        }


        [TestMethod]
        public void Validate_Reverse_String_Must_Return_Reversed()
        {
            var result1 = _service.ReverseWords("    ");
            var result2 = _reference.ReverseWords("    ");

            Assert.AreEqual(result1, result2);

            Assert.AreEqual(result1, "    ");
        }

        [TestMethod]
        public void Validate_Reverse_Empty_String_Must_Return_Empty_String()
        {
            var result1 = _service.ReverseWords("Peyman Malaei");
            var result2 = _reference.ReverseWords("Peyman Malaei");

            Assert.AreEqual(result1, result2);

            Assert.AreEqual(result1, "namyeP iealaM");
        }

        [TestMethod]
        public void Validate_Reverse_String_Must_Return_Exception()
        {
            try
            {
                var result1 = _service.ReverseWords(null);
            }
            catch (FaultException<ArgumentNullException> exp)
            {
                Assert.IsNotNull(exp.Message);
            }
            

            try
            {
                var result2 = _reference.ReverseWords(null);
            }
            catch (FaultException<ArgumentNullException> exp)
            {
                Assert.IsNotNull(exp.Message);
            }

            
        }

        [TestMethod]
        public void Validate_FibonacciNumber_Must_Return_Same_Value()
        {
            var result1 = _service.FibonacciNumber(16);
            var result2 = _reference.FibonacciNumber(16);

            Assert.AreEqual(result1, result2);

            Assert.AreEqual(result1, 987);
        }

        [TestMethod]
        public void Validate_FibonacciNumber_Zerp_Must_Return_Same_Value()
        {
            var result1 = _service.FibonacciNumber(0);
            var result2 = _reference.FibonacciNumber(0);

            Assert.AreEqual(result1, result2);

            Assert.AreEqual(result1, 0);
        }

        [TestMethod]
        public void Validate_FibonacciNumber_With_Negative_Must_Return_Exception()
        {
            try
            {
                var result1 = _service.FibonacciNumber(-93);
            }
            catch (FaultException<ArgumentOutOfRangeException> exp)
            {
                Assert.IsNotNull(exp);
            }
            catch (FaultException fe)
            {
                FaultCode fc = fe.Code;
                FaultReason fr = fe.Reason;

                var mf = fe.CreateMessageFault();
                if (mf.HasDetail)
                {
                    var detailedMessage = mf.GetDetail<ArgumentOutOfRangeException>();
                }
            }
        }

        [TestMethod]
        public void Validate_FibonacciNumber_Must_Return_Exception()
        {
            try
            {
                var result1 = _service.FibonacciNumber(100);
            }
            catch (FaultException exp)
            {
                Assert.IsNotNull(exp.Message);
            }
        }

        [TestMethod]
        public void Validate_WhoAreYou_Must_Return_Peymanmi_Email()
        {
            var result1 = _service.WhatIsYourToken();

            Assert.IsNotNull(result1);

            Assert.IsTrue(result1 != Guid.Empty);
        }

        public void Dispose()
        {
            if (_service != null)
                _service.Close();
            if (_reference != null)
                _reference.Close();
        }
    }
}
